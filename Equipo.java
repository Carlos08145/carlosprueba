/*
 *  
 */
public class Equipo{
	
	 private String placa, modelo, especificaciones, marca, estadoEquipo;
	 
 public Equipo (String placa, String marca,  String modelo, String especificaciones, String estadoEquipo ){
	 setPlaca(placa);
	 setMarca(marca);
	 setModelo(modelo);
	 setEspecificaciones(especificaciones);
	 setEstadoEquipo(estadoEquipo);
}
 public Equipo (){
	 setPlaca("");
	 setMarca("");
	 setModelo("");
	 setEspecificaciones("");
	 setEstadoEquipo("");
}
	//Set y Get
	public void setPlaca (String placa){
	this. placa = placa;
}
public  String getPlaca (){
	return placa;
}
public void setMarca (String marca){
	this. marca = marca;
}
public  String getMarca (){
	return marca;
}
public void setModelo (String modelo){
	this. modelo = modelo;
}
public  String getModelo (){
	return modelo;
}
public void setEspecificaciones (String especificaciones){
	this. especificaciones= especificaciones;
}
public  String getEspecificaciones (){
	return especificaciones;
}

public void setEstadoEquipo (String estadoEquipo){
	this. estadoEquipo = estadoEquipo;
}
public  String getEstadoEquipo (){
	return estadoEquipo;
}
public String toString (){
	return " El dispositivo seleccionado es "+ getMarca()+ " El modelo es "+getModelo()+ " las especificaciones son "+ getEspecificaciones()+ " el estado del equipo es "+ getEstadoEquipo() ;
}
}

