Git global setup
git config --global user.name "Carlos Montero"
git config --global user.email "carlos.monteroloaiza@ucr.ac.cr"

Create a new repository
git clone https://gitlab.com/Carlos08145/carlosprueba.git
cd carlosprueba
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/Carlos08145/carlosprueba.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/Carlos08145/carlosprueba.git
git push -u origin --all
git push -u origin --tag
