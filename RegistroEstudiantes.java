public class RegistroEstudiantes
{
private Estudiante estudiantes[ ];

public RegistroEstudiantes( ){
		estudiantes= new Estudiante[10];
}
public RegistroEstudiantes(int longitud){
	 estudiantes= new Estudiante[longitud];
}
public void setEstudiantes(Estudiante estudiante, int indice)
{
	
}

public int getIndice(String carne){
	for (int indice = 0; indice < estudiantes.length; indice++)
		{
			if (estudiantes[indice] != null)
			{
				if (estudiantes[indice] .getCarne().equals(carne))
				{
					return indice; 
				}
			}
		}
			return -1;
	
}

public Estudiante getEstudiante(String carne)
	{
		int indice = getIndice(carne);
		if (indice != -1)
		{
			return estudiantes[indice];
		}
		return null;
	}
	
	public Estudiante getEstudiante(int posicion)
	{
		return estudiantes[posicion];
	}
	
	public boolean eliminarEstudiante(int posicion)
	{ 
		if (estudiantes[posicion]!= null){
		estudiantes[posicion]= null;
		return true;
		}
	return false;
	}
	
	public int getLength( )
	{
		return estudiantes.length;
	}
	
	public boolean isEstudiante( String carne)
	{ 
		if (getEstudiante(carne)!= null)
		{
		return true;
		}
		return false;
	}
	public int getPosicionVacia(){
		for (int indice = 0; indice < estudiantes.length; indice++)
		{
			if (estudiantes[indice]== null){
			return indice;
			}
		}
		return -1;
	}
	public String toString(){
		String informacion="";
		for(int indice=0; indice<estudiantes.length; indice++){
			informacion+= estudiantes[indice].toString();
		}
	return informacion;
}
}
