/*
 * Estudiante.java
 * 
 */


public class Estudiante {
	private String nombre, carne, telefono, provincia;
	
public Estudiante() {
		
	}
public Estudiante(String nombre, String carne, String telefono, String provincia){
	nombre=nombre;
	carne=carne;
	telefono=telefono;
	provincia=provincia;
}
public void setNombre (String nombre){
	 nombre=nombre;
 }
public String getNombre (){
	return nombre;
}
public void setCarne (String carne){
	 carne=carne;
 }
public String getCarne (){
	return carne;
}
public void setTelefono (String telefono){
	 telefono=telefono;
 }
public String getTelefono (){
	return telefono;
}
public void setProvincia (String provincia){
	 provincia=provincia;
 }
public String getProvincia (){
	return provincia;
}
public String toString(){
	return "El nombre del estudiante es "+getNombre()+" \n el numero de cedula es "+getCarne()
	+" \n El numero de telefono es "+getTelefono()
	+" \n La provincia que resida es "+getProvincia();
}
}

